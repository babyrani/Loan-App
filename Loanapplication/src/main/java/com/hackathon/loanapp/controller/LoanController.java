package com.hackathon.loanapp.controller;
 
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.hackathon.loanapp.dao.Loan;
import com.hackathon.loanapp.service.LoanService;
import com.hackathon.loanapp.service.LoanServiceImpl;

@CrossOrigin
@RestController
@RequestMapping("/rest")
public class LoanController {

	LoanService loanService = new LoanServiceImpl();
	
	/*
	 *  POST request add customer load=ndetails into the Database 
	 * */
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public ResponseEntity<String> addCustomerLoanDetails(@RequestBody Loan loanEntity) {

		ResponseEntity<String> myResponse = null;

		if (loanService.addCustomerLoanDetails(loanEntity)) {
			myResponse = new ResponseEntity<String>(HttpStatus.OK);
		} else {
			myResponse = new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
		}
		return myResponse;
	}

}
