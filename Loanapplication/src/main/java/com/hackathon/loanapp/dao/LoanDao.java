package com.hackathon.loanapp.dao;

public interface LoanDao {

	public boolean getLoan(Loan loanApplication);
	
}
