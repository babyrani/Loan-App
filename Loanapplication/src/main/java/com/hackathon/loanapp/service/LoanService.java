package com.hackathon.loanapp.service;

import com.hackathon.loanapp.dao.Loan;
/**
 * LoanService interface with method signature
 */
public interface LoanService {

	boolean addCustomerLoanDetails(Loan loan);

}
