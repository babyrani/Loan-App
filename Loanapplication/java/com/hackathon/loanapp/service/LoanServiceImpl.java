package com.hackathon.loanapp.service;

import org.springframework.beans.factory.annotation.Autowired;

import com.hackathon.loanapp.dao.Loan;
import com.hackathon.loanapp.dao.LoanDao;
/**
 * LoanServiceImplementaion class
 */
public class LoanServiceImpl implements LoanService {

	@Autowired
	LoanDao loanDao;

	public boolean addCustomerLoanDetails(Loan loan) {
		return loanDao.getLoan(loan);
	}
}
